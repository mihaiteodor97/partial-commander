﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO.Compression;
using System.Windows.Media;

namespace Partial_Commander
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.LoadDirectoriesLeft();
            this.LoadDirectoriesRight();
        }

        public void LoadDirectoriesLeft()
        {
            var drives = DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                this.TreeViewLeft.Items.Add(this.GetItem(drive));
            }
        }

        public void LoadDirectoriesRight()
        {
            var drives = DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                this.TreeViewRight.Items.Add(this.GetItem(drive));
            }
        }

        private TreeViewItem GetItem(DriveInfo drive)
        {
            StackPanel stack = new StackPanel();
            stack.Orientation = Orientation.Horizontal;
            Label lbl = new Label();
            lbl.Content = drive.Name;
            Image image = new Image();
            image.Source = new BitmapImage
                (new Uri("C:\\Users\\mihai teodor\\Desktop\\mvp\\Partial_Commander\\Partial_Commander\\UnitBV.jpg"));
            stack.Children.Add(image);
            stack.Children.Add(lbl);

            var item = new TreeViewItem
            {
                Header = stack,
                DataContext = drive,
                Tag = drive
            };

            this.AddDummy(item);
            item.Expanded += new RoutedEventHandler(item_Expanded);
            return item;
        }

        private TreeViewItem GetItem(DirectoryInfo directory)
        {
            var item = new TreeViewItem
            {
                Header = directory.Name,
                DataContext = directory,
                Tag = directory
            };

            this.AddDummy(item);
            item.Expanded += new RoutedEventHandler(item_Expanded);
            return item;

        }

        private TreeViewItem GetItem(FileInfo file)
        {
            var item = new TreeViewItem
            {
                Header = file.Name,
                DataContext = file,
                Tag = file
            };


            return item;
        }

        private void ExploreDirectories(TreeViewItem item)
        {
            var directoryInfo = (DirectoryInfo)null;
            if (item.Tag is DriveInfo)
            {
                directoryInfo = ((DriveInfo)item.Tag).RootDirectory;
            }
            else if (item.Tag is DirectoryInfo)
            {
                directoryInfo = (DirectoryInfo)item.Tag;
            }
            else if (item.Tag is FileInfo)
            {
                directoryInfo = ((FileInfo)item.Tag).Directory;
            }
            if (object.ReferenceEquals(directoryInfo, null)) return;
            foreach (var directory in directoryInfo.GetDirectories())
            {
                var isHidden = (directory.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden;
                var isSystem = (directory.Attributes & FileAttributes.System) == FileAttributes.System;
                if (!isHidden && !isSystem)
                {
                    item.Items.Add(this.GetItem(directory));
                }
            }
        }

        private void ExploreFiles(TreeViewItem item)
        {
            var directoryInfo = (DirectoryInfo)null;
            if (item.Tag is DriveInfo)
            {
                directoryInfo = ((DriveInfo)item.Tag).RootDirectory;
            }
            else if (item.Tag is DirectoryInfo)
            {
                directoryInfo = (DirectoryInfo)item.Tag;
            }
            else if (item.Tag is FileInfo)
            {
                directoryInfo = ((FileInfo)item.Tag).Directory;
            }
            if (object.ReferenceEquals(directoryInfo, null)) return;
            foreach (var file in directoryInfo.GetFiles())
            {
                var isHidden = (file.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden;
                var isSystem = (file.Attributes & FileAttributes.System) == FileAttributes.System;
                if (!isHidden && !isSystem)
                {
                    item.Items.Add(this.GetItem(file));
                }
            }
        }


        void item_Expanded(object sender, RoutedEventArgs e)
        {
            var item = (TreeViewItem)sender;
            if (this.HasDummy(item))
           {
                this.Cursor = Cursors.Wait;
                this.RemoveDummy(item);
                this.ExploreDirectories(item);
                this.ExploreFiles(item);
                this.Cursor = Cursors.Arrow;
            }
        }


        private void AddDummy(TreeViewItem item)
        {
            item.Items.Add(new DummyTreeViewItem());
        }

        private bool HasDummy(TreeViewItem item)
        {
            return item.HasItems && (item.Items.OfType<TreeViewItem>().ToList().FindAll(tvi => tvi is DummyTreeViewItem).Count > 0);
        }

        private void RemoveDummy(TreeViewItem item)
        {
            var dummies = item.Items.OfType<TreeViewItem>().ToList().FindAll(tvi => tvi is DummyTreeViewItem);
            foreach (var dummy in dummies)
            {
                item.Items.Remove(dummy);
            }
        }


        private void PackButton_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem sursa = TreeViewLeft.SelectedItem as TreeViewItem;
            TreeViewItem destinatie = TreeViewRight.SelectedItem as TreeViewItem;

            string pathSursa, pathDestinatie, numeFisier;

            pathSursa = GetPathSursa(sursa);
            pathDestinatie = GetPathDestinatie(destinatie);
            numeFisier = sursa.Header.ToString();

            Compresie(pathSursa, pathDestinatie, numeFisier);

        }

        private void Compresie(string startPath, string zipPath, string fileName)
        {
            string zipName = zipPath + "\\";
            zipPath += "\\Rezultat";
            System.IO.Directory.CreateDirectory(zipPath);
            File.Copy(startPath, zipPath + "\\" + fileName);
            zipName += fileName;
            zipName += ".zip";
            ZipFile.CreateFromDirectory(zipPath, zipName);

            Directory.Delete(zipPath, true);



        }

        private void UnpackButton_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem sursa = TreeViewLeft.SelectedItem as TreeViewItem;
            TreeViewItem destinatie = TreeViewRight.SelectedItem as TreeViewItem;

            string pathSursa, pathDestinatie, numeFisier;

            pathSursa = GetPathSursa(sursa);
            pathDestinatie = GetPathDestinatie(destinatie);
            numeFisier = sursa.Header.ToString();

            Decompresie(pathSursa, pathDestinatie, numeFisier);

        }

        private void Decompresie(string zipPath, string extractPath, string numeFisier)
        {
            ZipFile.ExtractToDirectory(zipPath, extractPath );
        }


        public string GetPathSursa(TreeViewItem sursa)
        {
            string pathSursa = "";
            var directoryInfo = (DirectoryInfo)null;
            if (sursa.Tag is DriveInfo)
            {
                directoryInfo = ((DriveInfo)sursa.Tag).RootDirectory;
            }
            else if (sursa.Tag is DirectoryInfo)
            {
                directoryInfo = (DirectoryInfo)sursa.Tag;
            }
            else if (sursa.Tag is FileInfo)
            {
                directoryInfo = ((FileInfo)sursa.Tag).Directory;
            }

            foreach (var file in directoryInfo.GetFiles())
            {

                // Console.WriteLine($"{file.FullName}  {file.Name}   {sursa.Header}");
                if (sursa.Header.ToString() == file.Name)
                    pathSursa = file.FullName;

            }

            return pathSursa;
        }

        public string GetPathDestinatie(TreeViewItem destinatie)
        {
            string pathDest = "";
            var dir = (DirectoryInfo)null;
            
           dir = (DirectoryInfo)destinatie.Tag;
            Console.WriteLine($"dir: {dir.Name}  {dir.FullName}");
            pathDest = dir.FullName;

            return pathDest;
        }




        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButonMenuAbout_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AdresaMail_Click(object sender, RoutedEventArgs e)
        {
            Email fereastraEmail = new Email();
            fereastraEmail.Show();
        }

        private void Student_Click(object sender, RoutedEventArgs e)
        {
            Nume fereastraNume = new Nume();
            fereastraNume.Show();
        }

        private void CompareButton_Click(object sender, RoutedEventArgs e)
        {
            NotYetImplemented nyi = new NotYetImplemented();
            nyi.Show();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButonMenuView_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FullShowButton_Click(object sender, RoutedEventArgs e)
        {
            NotYetImplemented nyi = new NotYetImplemented();
            nyi.Show();
        }

        private void TreeViewButton_Click(object sender, RoutedEventArgs e)
        {
            NotYetImplemented nyi = new NotYetImplemented();
            nyi.Show();
        }

        private void VerticalViewButton_Click(object sender, RoutedEventArgs e)
        {
            Thickness margin = TreeViewLeft.Margin;
            margin.Left = 40;
            margin.Right = 40;
            margin.Top = 40;
            margin.Bottom = 240;
            TreeViewLeft.Margin = margin;

            Thickness margin2 = TreeViewRight.Margin;
            margin2.Left = 40;
            margin2.Right = 40;
            margin2.Top = 240;
            margin2.Bottom = 40;
            TreeViewRight.Margin = margin2;

            Thickness marginCopy = BtnCopy.Margin;
            marginCopy.Left = 100;
            marginCopy.Top = 210;
            BtnCopy.Margin = marginCopy;

            Thickness marginMove = BtnMove.Margin;
            marginMove.Left = 280;
            marginMove.Top = 210;
            BtnMove.Margin = marginMove;
        }

        private void NewTabButton_Click(object sender, RoutedEventArgs e)
        {
            NewTab nt = new NewTab();
            nt.Show();
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem sursa = TreeViewLeft.SelectedItem as TreeViewItem;
            TreeViewItem destinatie = TreeViewRight.SelectedItem as TreeViewItem;

            string pathSursa, pathDestinatie, numeFisier;

            pathSursa = GetPathSursa(sursa);
            pathDestinatie = GetPathDestinatie(destinatie);
            numeFisier = sursa.Header.ToString();
            File.Move(pathSursa, pathDestinatie + "\\" + numeFisier);
        }

        private void BtnMove_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem sursa = TreeViewLeft.SelectedItem as TreeViewItem;
            TreeViewItem destinatie = TreeViewRight.SelectedItem as TreeViewItem;

            string pathSursa, pathDestinatie, numeFisier;

            pathSursa = GetPathSursa(sursa);
            pathDestinatie = GetPathDestinatie(destinatie);
            Console.WriteLine($"destinatie: {pathDestinatie}");
            numeFisier = sursa.Header.ToString();
            File.Copy(pathSursa, pathDestinatie + "\\copy" + numeFisier);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem sursa = TreeViewLeft.SelectedItem as TreeViewItem;
            string pathSursa = GetPathSursa(sursa);
            File.Delete(pathSursa);
        }
    }


}
